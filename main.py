import pandas as pd
import matplotlib.pyplot as plt
import json
import seaborn as sns
from youtube_data import youtube_search_by_id, test_api

if  __name__ == "__main__":
    data = test_api("hfAQdY-9biQ")
    #print(data)
    json_data = json.dumps(data, indent=4, sort_keys=True)
    f  = open ("data.json", "w")
    f.write(json_data)
    f.close